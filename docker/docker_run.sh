#!/bin/bash
IMAGENAME=ykk_test
VERSION=latest
ID=$(hostname)-${USER//[^a-zA-Z0-9]/-}
IMAGE=gpu-registry:5000/$USER/$IMAGENAME:$VERSION
MEMORY=8g # 目安は実装サイズの半分としてください gpu-1050ti-[1,2,3]=8GB, gpu-1080-1=32GB, gpu-1080ti-[1,2]=64GB

docker container run \
  --detach \
  --hostname=$ID \
  --memory=$MEMORY \
  --name=$ID \
  --publish-all \
  --rm \
  -v /home/aoi.okehara/ykk_test_app/data/decision/dome/phase1:/app/src/test_match_decision/phase1 \
  -v /home/aoi.okehara/ykk_test_app/data/decision/dome/phase2:/app/src/test_match_decision/phase2 \
  -v /home/aoi.okehara/ykk_test_app/data/image/dome/phase1:/app/src/test_match_image/phase1 \
  -v /home/aoi.okehara/ykk_test_app/data/image/dome/phase2:/app/src/test_match_image/phase2 \
  -v /home:/home \
  -it \
  $IMAGE \
  /bin/bash
