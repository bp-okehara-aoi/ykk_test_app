import os
import cv2
import pytest
import numpy as np


def get_not_matching_images(datadir, ph1_dir, ph2_dir):
    #ファイル名の取得
    ph1_files = sorted(os.listdir(os.path.join(str(datadir),ph1_dir)))
    ph2_files = sorted(os.listdir(os.path.join(str(datadir),ph2_dir)))
    files = set(ph1_files) & set(ph2_files)
    #ファイル毎の値比較
    diff_images=[]
    for filename in files:
        im1 = cv2.imread(str(os.path.join(str(datadir), ph1_dir, filename)))
        im2 = cv2.imread(str(os.path.join(str(datadir), ph2_dir, filename)))
        if not np.array_equal(im1, im2):
            diff_images.append(filename)
    return diff_images    


@pytest.mark.skip(reason='pytestskip')
def test_bar_match_image(datadir):
    '''
    bar照明のphase1予測画像とphase2予測画像が一致してるか確認するテスト(領域別には出せないので通しのテストデータを利用すること)
    '''
    diff_images = get_not_matching_images(datadir, 'phase1/bar', 'phase2/bar')
    assert len(diff_images) == 0


@pytest.mark.skip(reason='pytestskip')
def test_dome_match_image(datadir):
    '''
    dorm照明のphase1予測画像とphase2予測画像が一致してるか確認するテスト(領域別には出せないので通しのテストデータを利用すること)
    '''
    diff_images = get_not_matching_images(datadir, 'phase1/dome', 'phase2/dome')
    assert len(diff_images) == 0
