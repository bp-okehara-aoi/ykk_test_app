import pandas as pd
import pytest

def create_decision_comparison_df(phase1_path, phase2_path):
    with open(phase1_path, 'r') as f:
        ph1_df = pd.read_csv(f)
    df_lst = []
    for row in range(ph1_df.shape[0]-1):
        ph1_y_pred = [int(i) for i in ph1_df.iloc[row,-2:]['y_predict'].strip('[').strip(']').split(',')]
        ph1_filename = [str(i.split('/')[-1].strip("'")) for i in ph1_df.iloc[row,-2:]['image names'].strip('[').strip(']').split(',')]
        ph1_df_tmp = pd.DataFrame([ph1_filename, ph1_y_pred]).T
        df_lst.append(ph1_df_tmp)
    ph1_df =pd.concat(df_lst)
    ph1_df.columns =  ['filename', 'ph1_pred']

    #phase2データの読み込み
    with open(phase2_path,'r') as f:
            ph2_df = pd.read_csv(f)
    ph2_df.columns = ['filename', 'ph2_pred']
    #比較用データフレーム作成
    df_ph1_ph2 = pd.merge(ph1_df.drop_duplicates(subset=['filename', 'ph1_pred']), ph2_df, on='filename')
    df_ph1_ph2['diff'] = df_ph1_ph2['ph1_pred'] !=  df_ph1_ph2['ph1_pred']
    return df_ph1_ph2


@pytest.mark.skip(reason='pytestskip')
def test_bar_right_decision(datadir):
    '''
    bar照明のright領域のphase2の予測結果が、Phase1の予測結果と一致するか確認するテスト
    '''
    df = create_decision_comparison_df(str(datadir/'phase1/bar_right.csv'), str(datadir/'phase2/bar_right.csv'))
    assert df['diff'].sum() == 0


@pytest.mark.skip(reason='pytestskip')
def test_bar_tape_decision(datadir):
    '''
    bar照明のtape領域のphase2の予測結果が、Phase1の予測結果と一致するか確認するテスト
    '''
    df = create_decision_comparison_df(str(datadir/'phase1/bar_tape.csv'), str(datadir/'phase2/bar_tape.csv'))
    assert df['diff'].sum() == 0

@pytest.mark.skip(reason='pytestskip')
def test_dome_right_decision(datadir):
    '''
    dome照明のright領域のphase2の予測結果が、Phase1の予測結果と一致するか確認するテスト
    '''
    df = create_decision_comparison_df(str(datadir/'phase1/dome_right.csv'), str(datadir/'phase2/dome_right.csv'))
    assert df['diff'].sum() == 0

@pytest.mark.skip(reason='pytestskip')
def test_dome_tape_decision(datadir):
    '''
    dome照明のtape領域のphase2の予測結果が、Phase1の予測結果と一致するか確認するテスト
    '''
    df = create_decision_comparison_df(str(datadir/'phase1/dome_tape.csv'), str(datadir/'phase2/dome_tape.csv'))
    assert df['diff'].sum() == 0

@pytest.mark.skip(reason='pytestskip')
def test_dome_left_decision(datadir):
    '''
    dome照明のtape領域のphase2の予測結果が、Phase1の予測結果と一致するか確認するテスト
    '''
    df = create_decision_comparison_df(str(datadir/'phase1/dome_left.csv'), str(datadir/'phase2/dome_left.csv'))
    assert df['diff'].sum() == 0